﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta id="myViewport" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" />

    <link rel="icon" href="favicon.ico">

    <title>AIA Sharing a Life</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap-3.2.0-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery-1.11.1.min.js"></script>
    <script src="assets/bootstrap-3.2.0-dist/js/bootstrap.js"></script>
    <script src="assets/js/script.js"></script>

    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAgTRXFlEtmX91iUxqxYjX96ksQo70c84g">
    </script>

    <script type="text/javascript">

        //<![CDATA[

        var s_UserId = "";

        var amc = amc || {};

        if (!amc.on) {

            amc.on = amc.call = function() {

            }

        };

        document

            .write("<scr"+"ipt type=\"text/javascript\" src=\"//www.adobetag.com/d1/v2/ZDEtYWlhZ3JvdXAtMTA2ODktMTczMi0=/amc.js\"></sc"+"ript>");

        //]]>

    </script>

    <script>
        var hideReg = true;
        var fullGallery = true;
    </script>
</head>

<!--<body data-spy="scroll" data-target="#navbar">-->
<body>
<!-- Fixed navbar -->
<div id="navbar" class="nav navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container inner-container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="assets/img/event-logo.png"> </a>
        </div>
        <div class="navbar-collapse collapse">

            <ul class="nav navbar-nav navbar">
                <li><a class="nav-link" data-target="#sec-header">รายละเอียดงาน</a></li>
                <li><a class="nav-link" data-target="#sec-map">แผนที่</a></li>
                <li><a class="nav-link" data-target="#sec-register">ลงทะเบียน</a></li>
                <li><a class="nav-link" data-target="#sec-gallery2">แกลเลอรี่</a></li>
                <!--                <li class="active"><a href="./">Fixed top</a></li>-->
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a class="navbar-aiabrand" href="http://www.aia.co.th/"><img src="assets/img/aia-logo.png"></a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>

<!--<div class="container-scale">-->
<div class="container-fluid">
    <div id="sec-header" class="row sec-block">

        <div class="pallalax-bg col-md-12"></div>

        <div class="header-text col-md-12">
            <div style="height: 100px"></div>
            <div id="event" class="inner-container">
                <h1> 11 ตุลาคมนี้ รวมพลังทำดี เพื่อตอบแทนสังคมไทย </br> กับ AIA SHARING A LIFE </h1>
                <p><b> เลือกลงทะเบียนเพื่อเข้าร่วมกิจกรรมกับเราได้ใน 7 จุดทั่วกรุงเทพฯ และ 14 จุดในต่างจังหวัด </b></p>
                <p>ติดตามข้อมูลกิจกรรมล่าสุดได้ที่ &nbsp&nbsp&nbsp
                    <a href="https://www.facebook.com/ThailandAIA" target="_blank"><img src="assets/img/aia-facebook.png"/></a>
                    </br> </br>
                </p>
            </div>
        </div>
    </div>


        <!-- video -->

    <div class="row">
        <div class="sec-seperator col-md-12"> </div>
    </div>

    <div id="sec-video" class="sec-block">
        <div class="inner-container">
            <div class="row">

                <div class="logo-row col-md-12">


                </div>
                <div class="col-md-8 col-xs-12">
<!--                    <video width="100%"  controls>-->
<!--                        <source src="assets/video/video1.mp4" type="video/mp4">-->
<!--                        Your browser does not support the video tag.-->
<!--                    </video>-->
                    <iframe id="youtubeVideoFrame" width="100%" height="400px" allowfullscreen
                            src="http://www.youtube.com/embed/ZUVBDOLICaw">
                    </iframe>
                    <div id="videoCaption" class="play-video-caption">
                        AIA SHARING A LIFE: It's time to share
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <!--                <ul class="nav nav-pills nav-stacked video-list-hr">-->
                    <!--                    <li class="video-thumb active" data-video-src="video 1">-->
                    <!--                        <div class="video-thumb-block">-->
                    <!--                            <div class="video-thumb-img"></div>-->
                    <!--                            <p class="video-thumb-caption">It's time to share</p>-->
                    <!---->
                    <!--                        </div>-->
                    <!--                    </li>-->
                    <!--                    <li class="video-thumb" data-video-src="video 2">-->
                    <!--                        <div class="video-thumb-block">-->
                    <!--                            <div class="video-thumb-img"></div>-->
                    <!--                            <p class="video-thumb-caption">It share to time</p>-->
                    <!---->
                    <!--                        </div>-->
                    <!--                    </li>-->
                    <!--                    <li class="video-thumb" data-video-src="video 3">-->
                    <!--                        <div class="video-thumb-block">-->
                    <!--                            <div class="video-thumb-img"></div>-->
                    <!--                            <p class="video-thumb-caption">It share to time 3</p>-->
                    <!---->
                    <!--                        </div>-->
                    <!--                    </li>-->
                    <!--                    <li class="video-thumb" data-video-src="video 4">-->
                    <!--                        <div class="video-thumb-block">-->
                    <!--                            <div class="video-thumb-img"></div>-->
                    <!--                            <p class="video-thumb-caption">It share to time 4</p>-->
                    <!---->
                    <!--                        </div>-->
                    <!--                    </li>-->
                    <!---->
                    <!--                </ul>-->
                    <ul class="nav nav-pills nav-stacked video-list">
                        <li class="video-thumb active" data-video-src="ZUVBDOLICaw" data-video-caption="AIA Sharing a Life - It's time to share">
                            <div class="video-thumb-block">
                                <div style="background-image: url('http://img.youtube.com/vi/ZUVBDOLICaw/0.jpg')" class="video-thumb-img"></div>
                                <p class="video-thumb-caption">It's time to share</p>

                            </div>
                        </li>
                        <li class="video-thumb" data-video-src="DP-H5nxpPpE" data-video-caption="AIA Sharing a Life - Share Together">
                            <div class="video-thumb-block">
                                <div style="background-image: url('http://img.youtube.com/vi/DP-H5nxpPpE/0.jpg')" class="video-thumb-img"></div>
                                <p class="video-thumb-caption">Share Together </p>

                            </div>
                        </li>

                    </ul>
                </div>

            </div>
            </br>
        </div>

    </div>


    <div class="row">
        <div class="sec-seperator col-md-12">
    </div>
    </div>
    <div id="sec-map" class="sec-block">

        <div class="pallalax-bg col-md-12">

        </div>

        <div class="inner-container">
            <div class="map-header">
                <h1>แผนที่จัดกิจกรรมทั่วประเทศ</h1>
                <p>คลิกเลือกสถานที่ที่คุณสนใจร่วมกิจกรรมกับเรา</p>
            </div>

            <div class="map-container center-children">
                <div class="map-area">

                    <img id="mapImg" class="img-rounded img-responsive" src="assets/img/map.png" alt="...">


                    <?php
                    global $pinCounter;
                    global $pinData;
                    $pinCounter = 0;
                    $pinData = array();

                    function addPinData($t , $x , $y  , $placeList){
                        global $pinCounter,$pinData;
                        // minus offset
                        $x -= 12;
                        $y -= 30;
                        $style = "left:{$x}px ; top: {$y}px";
                        //data-toggle="popover" title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?"

                        $output = '';
                        $output .= '<img class="map-pin" style="'.$style.'" src="assets/img/pin_dark.png" pin-id="'.$pinCounter.'" ';
                        $output .= 'data-toggle="popover"  title="'.$t.'"/>';
                        echo $output;


                        $p = new stdClass();
                        $p->title = trim($t);
                        $p->x = $x;
                        $p->y = $y;
                        $p->pinId = $pinCounter;
                        $p->placeList = (object)$placeList;


                        $pinData[$pinCounter] = $p;
                        $pinCounter += 1;
                        return $p;
                    };

                    // ------------------------------------------- //
                    // to be seriallize
                    include_once('record.php');
                    $rawData = record::getLocationList();

                    // ------------------------------------------- //
                    foreach( $rawData as $raw){
                        addPinData( $raw[0], $raw[1] , $raw[2] , $raw[3]);
                    }


                    $pinData = json_encode($pinData);
                    echo "<script> var pinData = {$pinData} </script>";
                    ?>

                    <!-- Modal -->
                    <div id="mapPopover" class="popover fade in">
                        <h3 class="popover-title"></h3>
                        <div class="popover-content"></div>
                    </div>

                </div>
            </div>
        </div>




    </div>

    <div class="row">
        <div class="sec-seperator col-md-12"></div>
    </div>
    <div id="sec-register" class="row sec-block">

        <div class="pallalax-bg col-md-12"></div>


        <div class="register-header">
            <h1 class="inner-container">ลงทะเบียน</h1>
        </div>
        <div id="register" class="inner-container center-children" >
            <div class="inner-area">

                <form id="register-form" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-4 control-label">ชื่อ นามสกุล</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputName" placeholder="">
                        </div>
                        <div class="col-sm-offset-4 col-sm-8">
                            <div id="register-id-type" class="dropdown">
                                <p id="dropdownMenu1" data-toggle="dropdown">
                                    กรุณาเลือก

                                </p>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                    <li data-id="0" role="presentation"><p role="menuitem" tabindex="-1" href="">ประชาชนทั่วไป</p></li>
                                    <li data-id="1" role="presentation"><p role="menuitem" tabindex="-1" href="">ลูกค้า AIA</p></li>
                                    <li data-id="2" role="presentation"><p role="menuitem" tabindex="-1" href="">ตัวแทน AIA</p></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="form-group"> <!-- show error example -->
                        <label for="inputId" class="col-sm-4 control-label">หมายเลขรหัส</label>
                        <div class="col-sm-8">
                            <input id="inputId" type="text" maxlength="13" class="form-control numberonly"  placeholder="">
                        </div>
                        <div class="col-sm-offset-4 col-sm-8">
                            <p id="register-id-type-label">เลขที่บัตรประชาชน</p>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="inputTel" class="col-sm-4 control-label">เบอร์โทรศัพท์</label>
                        <div class="col-sm-8">
                            <input id="inputTel" type="text" maxlength="10" class="form-control numberonly"   placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-sm-4 control-label">อีเมล</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="inputEmail" placeholder="">
                        </div>
                    </div>

                    <div class="form-group">

                    </div>
                </form>
                <div class="col-sm-offset-4 col-sm-8">
                    <button id="registerBtn" class="aia-button">สถานที่ร่วมกิจกรรม</button>
                </div>
            </div>

        </div>

        <div id="register-complete" class="inner-container center-children" style="display:none">
            <div class="inner-area">
                <h1>
                    ขอขอบคุณที่ลงทะเบียนร่วมกิจกรรมกับเรา
                </h1>
                <h2>
                    เจอกัน 11 ตุลาคม 2557
                    วันที่เราจะรวมพลังทำดี เพื่อตอบแทนสังคมไทยอย่างยิ่งใหญ่
                </h2>
                <div class="center-children">
                    <img src="assets/img/event-logo-l.png"/>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="sec-seperator col-md-12"></div>
    </div>
    <div id="sec-gallery" class="inner-container" style="display:none">
        <h1>แกลเลอรี่</h1>
        <div class="row">
            <?php


            if ($handle = opendir('assets/photo')) {

                /* This is the correct way to loop over the directory. */
                while (false !== ($entry = readdir($handle))) {
                    if( $entry == '.' || $entry == '..') continue;

                    echo '<div class="col-xs-6 col-md-3">';
                    echo '<div class="thumbnail" data-toggle="modal" data-target="#galleryPopup">';
                    echo '  <img src="assets/photo/'.$entry.'" alt="...">';
                    //echo '  <div class="thumb-img" style="background-image:url(assets/photo/'.$entry.')"></div>';

                    echo '</div></div>';
                }

                closedir($handle);
            }

            ?>
        </div>
    </div>

    <div id="sec-gallery2" class="inner-container">
        <h1>แกลเลอรี่</h1>
        <div class="row">
            <?php

//            include_once('record.php');
//            $r = new record();
//            $list = $r->getApprovedList(0 , 20);
//
//            $data = array();
//            foreach( $list as $item){
//                $path = $item->external_id.'.jpg';
//
//                echo '<div class="col-xs-6 col-md-3">';
//                echo '<div class="thumbnail" data-toggle="modal" data-target="#galleryPopupFull" data-photo-id="'.$item->id.'">';
//                echo '  <img src="data/img/'.$path.'" alt="...">';
//                echo '</div></div>';
//
//                $data[$item->id] = $item;
//            }
//
//
//            echo "<script>  galData = ".json_encode($data)." </script>";

            ?>
        </div>
    </div>


    <div id="gallery-loading" class="row">
        Loading...
    </div>

    <div class="row">
        <div id='sec-ending' class="ending col-md-12">

            <div class="white-area row center-children" >
                <h1>#AIASHARINGALIFE #AIASAL</h1>
            </div>

            <div class="red-area row">
                <div class="inner-container" style="padding-top: 15px">
                    <div class="col-md-2" style="padding-left: 0px">
                        <a class="" href="#"><img src="assets/img/event-logo.png"> </a>
                    </div>
                    <div class="col-md-8">
                        <p class="text-align-center" style="padding-top: 5px"> สงวนลิขสิทธิ์ © 2557, กลุ่มบริษัทเอไอเอ และบริษัทในเครือ ขอสงวนสิทธิ์ทั้งหลายตามกฎหมาย </p>
                    </div>
                    <div class="col-md-2 hidden-xs" style="padding-right: 0px">
                        <a href="http://www.aia.co.th/"><img style="float:right;" src="assets/img/aia-logo.png"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div> <!-- /container -->
<!--</div> <!-- scale -->


<!-- Modal -->
<div class="modal fade" id="mapPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12"> <h2 class="modal-title" id="myModalLabel">เลือกกิจกรรมที่ใช่ โลเคชั่นที่ชอบ</h2></div>
                    <div class="col-xs-12 col-md-4">
                        <div id="main-location" class="main-location dropdown">
                            <p id="dLabel" role="button" data-toggle="dropdown" data-target="#">
                                7 จุดในกรุงเทพฯ
                            </p>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <li data-location-id="0-0"> 7 จุดในกรุงเทพฯ </li>
                                <li data-location-id="1-0"> 14 จุดทั่วประเทศ </li>
                            </ul>
                        </div></div>
                    <div class="col-xs-12 col-md-8">
                        <div id="sub-location1" class="sub-location dropdown">
                            <p data-toggle="dropdown" href="#"></p>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            </ul>
                        </div>

                        <div id="sub-location2" class="sub-location dropdown">
                            <p data-toggle="dropdown" href="#"></p>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="map-container" >

                    <div id="map-canvas"></div>
                    <div class="map-footer">
                        <p> </p>
                    </div>
                </div>

                <div class="map-button-area center-children">
                    <a id="mapRegisterBtn" class="nav-link" data-target="#sec-register"><button class="aia-button"> ลงทะเบียน </button></a>
                    <a id="mapConfirmBtn" class="nav-link" data-target="#sec-register" style="display:none"><button class="aia-button"> ยืนยันลงทะเบียน </button></a>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="galleryPopupFull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <!--                <div class="modal-header">-->
            <!--                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>-->
            <!--                    <h4 class="modal-title" id="myModalLabel">Title</h4>-->
            <!--                </div>-->
            <div class="modal-body">
                <div class="row">
                    <div class="header-bg"></div>
                    <img class="display col-xs-12 col-md-7" src=""/>
                    <div class="detail col-xs-12 col-md-5">
                        <div class="header">
                            <div class="user"></div>
                            <span>AIA SHARINGALIFE</span>
                            <button style="padding: 10px" type="button" class="close" data-dismiss="modal"><p aria-hidden="true">X</br></p><span class="sr-only">Close</span></button>
                        </div>
                        <div class="content"></div>
                        <div class="footer">
<!--                            <a href="#" target="_blank" class="footer-like"> 100 like </a>-->
                            <div class="footer-like"><a target="_blank" href=""><img src="assets/img/in-like.png"> <span></span></a> </div>
                            <div class="footer-logo"></div>
                        </div>
                    </div>
                </div>




            </div>
        </div>

        <img class="gal-arrow left new" src="assets/img/arrow-left.png">
        <img class="gal-arrow right new" src="assets/img/arrow-right.png">
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="galleryPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--                <div class="modal-header">-->
            <!--                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>-->
            <!--                    <h4 class="modal-title" id="myModalLabel">Title</h4>-->
            <!--                </div>-->


            <div class="modal-body">
                <button style="padding: 10px" type="button" class="close" data-dismiss="modal"><p aria-hidden="true">X</br></p><span class="sr-only">Close</span></button>
                <img class="display" src=""/>
            </div>
        </div>
        <img class="gal-arrow left old" src="assets/img/arrow-left.png">
        <img class="gal-arrow right old" src="assets/img/arrow-right.png">
    </div>
</div>

<div id="side-tab-fixed" class="hidden-xs">
   <a class="nav-link" data-target="#sec-register"> <img src="assets/img/side-tab.png" /> </a>
</div>

<!-- SiteCatalyst code version: H.22.1.
	

Copyright 1996-2011 Adobe, Inc. All Rights Reserved
	

More info available at http://www.omniture.com -->
	

<script src="http://www.aia.co.th/th/resources/9157278049be8400bf93ffb927f11de2/s_code_th.js" type="text/javascript"></script>
	

<script language="JavaScript" type="text/javascript"><!--
	

/* You may give each page an identifying name, server, and channel on
	

the next lines. */
	

s.pageName=""
	

s.channel=""
	

s.prop1=""
	

s.prop2=""
	

s.prop3=""
	
	

/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
	

var s_code=s.t();if(s_code)document.write(s_code)//--></script>
	

<script language="JavaScript" type="text/javascript"><!--
	

if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')
	

//--></script>
	

<noscript>
	

<img src="http://aiagroup.d1.sc.omtrdc.net/b/ss/aia-global/1/H.22.1--NS/0"
	

height="1" width="1" border="0" alt="" />
	

</noscript>
	

<!--/DO NOT REMOVE/-->
	

<!-- End SiteCatalyst code version: H.22.1. -->
	

<table width="820" border="0" cellspacing="0" cellpadding="10">
	

<tr>
	

<td></td>
	

</tr>
	

</table>
	

<!-- SiteCatalyst code version: H.22.1.
	

Copyright 1996-2011 Adobe, Inc. All Rights Reserved
	

More info available at http://www.omniture.com -->
	

<script src="/th/resources/9157278049be8400bf93ffb927f11de2/s_code_th.js" type="text/javascript"></script>
	

<script language="JavaScript" type="text/javascript"><!--
	

/* You may give each page an identifying name, server, and channel on
	

the next lines. */
	

s.pageName=""
	

s.channel=""
	

s.prop1=""
	

s.prop2=""
	

s.prop3=""
	
	

/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
	

var s_code=s.t();if(s_code)document.write(s_code)//--></script>
	

<script language="JavaScript" type="text/javascript"><!--
	

if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')
	

//--></script><noscript><img src="http://aiagroup.d1.sc.omtrdc.net/b/ss/aia-global/1/H.22.1--NS/0"
	

height="1" width="1" border="0" alt="" /></noscript>
	
	

<!-- End SiteCatalyst code version: H.22.1. -->

</body>
</html>
