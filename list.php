<?php

    session_start();
    if(!isset($_SESSION['username'])) {
        echo "Please login";
        header( "refresh:1;url=login.php" );
        exit();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Registration - AIA Sharing a Life</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap-3.2.0-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/dashboard.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">AIA Sharing a Life</a>
        </div>
        <div class="navbar-collapse collapse">
            <form class="navbar-form navbar-right">
<!--                <input type="text" class="form-control" placeholder="Search...">-->
                <a href="export.php"> <button type="button" class="btn btn-primary" >Export to CSV</button> </a>
            </form>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
<!--        <div class="col-sm-3 col-md-2 sidebar">-->
<!--            <ul class="nav nav-sidebar">-->
<!--                <li class="active"><a href="#">Overview</a></li>-->
<!--                <li><a href="#">Reports</a></li>-->
<!--                <li><a href="#">Analytics</a></li>-->
<!--                <li><a href="#">Export</a></li>-->
<!--            </ul>-->
<!--            <ul class="nav nav-sidebar">-->
<!--                <li><a href="">Nav item</a></li>-->
<!--                <li><a href="">Nav item again</a></li>-->
<!--                <li><a href="">One more nav</a></li>-->
<!--                <li><a href="">Another nav item</a></li>-->
<!--                <li><a href="">More navigation</a></li>-->
<!--            </ul>-->
<!--            <ul class="nav nav-sidebar">-->
<!--                <li><a href="">Nav item again</a></li>-->
<!--                <li><a href="">One more nav</a></li>-->
<!--                <li><a href="">Another nav item</a></li>-->
<!--            </ul>-->
<!--        </div>-->
        <div class="col-md-12 main">
            <h1 class="page-header">Registration List</h1>
<!---->
<!--            <div class="row placeholders">-->
<!--                <div class="col-xs-6 col-sm-3 placeholder">-->
<!--                    <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="Generic placeholder thumbnail">-->
<!--                    <h4>Label</h4>-->
<!--                    <span class="text-muted">Something else</span>-->
<!--                </div>-->
<!--                <div class="col-xs-6 col-sm-3 placeholder">-->
<!--                    <img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="Generic placeholder thumbnail">-->
<!--                    <h4>Label</h4>-->
<!--                    <span class="text-muted">Something else</span>-->
<!--                </div>-->
<!--                <div class="col-xs-6 col-sm-3 placeholder">-->
<!--                    <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="Generic placeholder thumbnail">-->
<!--                    <h4>Label</h4>-->
<!--                    <span class="text-muted">Something else</span>-->
<!--                </div>-->
<!--                <div class="col-xs-6 col-sm-3 placeholder">-->
<!--                    <img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="Generic placeholder thumbnail">-->
<!--                    <h4>Label</h4>-->
<!--                    <span class="text-muted">Something else</span>-->
<!--                </div>-->
<!--            </div>-->

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Type</th>
                        <th>ID</th>
                        <th>Tel</th>
                        <th>email</th>
                        <th>Province</th>
                        <th>Area</th>
                        <th>Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    include_once('record.php');

                    $rec = new record();
                    $rec->getFullList();

                    ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery-1.11.1.min.js"></script>
<script src="assets/bootstrap-3.2.0-dist/js/bootstrap.js"></script>
</body>
</html>
